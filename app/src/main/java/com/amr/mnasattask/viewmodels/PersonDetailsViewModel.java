package com.amr.mnasattask.viewmodels;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.amr.mnasattask.BR;
import com.amr.mnasattask.models.PersonModel;
import com.amr.mnasattask.views.adapters.DetailsAdapter;
import com.amr.mnasattask.views.adapters.PersonDetailsAdapter;

import java.util.ArrayList;

public class PersonDetailsViewModel extends BaseObservable implements PersonModel.IPersonModel {

    public PersonDetailsAdapter adapter;
    private PersonModel personInfo;
    private Context context;


    public PersonDetailsViewModel(Context context) {
        this.context = context;

        personInfo = new PersonModel(context);

        personInfo.setIPersonModel(this);
    }

    public void getData(long personId) {
        personInfo.getPersonInfo(personId);
    }

    @Bindable
    public PersonDetailsAdapter getAdapter() {
        return adapter;
    }

    @Override
    public void setPersonModels(ArrayList<PersonModel> personModels) { }

    @Override
    public void setPersonInfo(PersonModel personInfo) {
        this.personInfo = personInfo;
        this.adapter = new PersonDetailsAdapter(context, this.personInfo);

        notifyPropertyChanged(BR.adapter);
    }
}
