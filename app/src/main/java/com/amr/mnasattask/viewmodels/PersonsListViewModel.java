package com.amr.mnasattask.viewmodels;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.amr.mnasattask.BR;
import com.amr.mnasattask.models.PersonModel;
import com.amr.mnasattask.views.adapters.PersonsListAdapter;

import java.util.ArrayList;

public class PersonsListViewModel extends BaseObservable implements PersonModel.IPersonModel {

    private ArrayList<PersonModel> personModels;

    public PersonsListAdapter adapter;

    private PersonModel personModel;
    private Context context;

    public PersonsListViewModel(Context context) {
        this.context = context;

        personModel = new PersonModel(context);
        personModel.setIPersonModel(this);

        personModels = new ArrayList<>();
    }

    public void getData() {
        personModel.getPersonsModels();
    }


    @Bindable
    public PersonsListAdapter getAdapter() {
        return this.adapter;
    }

    @Override
    public void setPersonModels(ArrayList<PersonModel> personModels) {
        this.personModels = personModels;
        this.adapter = new PersonsListAdapter(context, this.personModels);

        notifyPropertyChanged(BR.adapter);
    }

    @Override
    public void setPersonInfo(PersonModel personInfo) { }
}
