package com.amr.mnasattask.models;

import android.content.Context;
import android.util.Log;

import com.amr.mnasattask.BuildConfig;
import com.amr.mnasattask.utilities.EndPoints;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class ApiConnection {

    private IApiConnection iApiConnection;
    private Gson gson;
    private Context context;

    public ApiConnection(Context context) {
        this.context = context;

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("M/d/yy hh:mm a");
        gson = gsonBuilder.create();
    }

    public void getPersonsList() {
        RequestQueue queue = Volley.newRequestQueue(context);

        StringRequest request = new StringRequest(
                Request.Method.GET,
                EndPoints.PERSONS_LIST_URL + BuildConfig.THE_MOVIE_DB_API_KEY,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            ArrayList<PersonModel> personModels = new ArrayList<>(Arrays.asList(
                                    gson.fromJson(new JSONObject(response).getJSONArray("results").toString(),
                                            PersonModel[].class)));
                            iApiConnection.passPersonsModels(personModels);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });

        queue.add(request);
    }

    public void getPersonInfo(long personId) {
        RequestQueue queue = Volley.newRequestQueue(context);

        StringRequest request = new StringRequest(
                Request.Method.GET,
                EndPoints.PERSON_MAIN_INFO.replace("person_id", String.valueOf(personId)) + BuildConfig.THE_MOVIE_DB_API_KEY,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            PersonModel personModel = gson.fromJson(response, PersonModel.class);
                            getPersonImages(personModel);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });

        queue.add(request);
    }

    public void getPersonImages(final PersonModel personModel) {
        RequestQueue queue = Volley.newRequestQueue(context);

        StringRequest request = new StringRequest(
                Request.Method.GET,
                EndPoints.PERSON_Images.replace("person_id", String.valueOf(personModel.getId())) + BuildConfig.THE_MOVIE_DB_API_KEY,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            ArrayList<Profile> profiles
                                    = new ArrayList<>(Arrays.asList(
                                            gson.fromJson(
                                                    new JSONObject(response).getJSONArray("profiles").toString(), Profile[].class))
                            );
                            personModel.setProfiles(profiles);
                            iApiConnection.passPersonModel(personModel);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });

        queue.add(request);
    }

    public void setIApiConnection(IApiConnection iApiConnection) {
        this.iApiConnection = iApiConnection;
    }

    public interface IApiConnection {
        void passPersonsModels(ArrayList<PersonModel> personModels);
        void passPersonModel(PersonModel personModel);
    }
}
