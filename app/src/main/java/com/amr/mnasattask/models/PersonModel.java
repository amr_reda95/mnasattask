package com.amr.mnasattask.models;


import android.content.Context;

import java.util.ArrayList;

public class PersonModel implements ApiConnection.IApiConnection {
    private long id;
    private float popularity;
    private String name, profile_path, birthday, known_for_department, place_of_birth;
    private ArrayList<Profile> profiles = new ArrayList<>();

    private transient ApiConnection apiConnection;
    private transient IPersonModel iPersonModel;
    private transient Context context;

    public PersonModel(Context context) {
        this.context = context;
        apiConnection = new ApiConnection(context);

        apiConnection.setIApiConnection(this);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public float getPopularity() {
        return popularity;
    }

    public void setPopularity(float popularity) {
        this.popularity = popularity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile_path() {
        return profile_path;
    }

    public void setProfile_path(String profile_path) {
        this.profile_path = profile_path;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getKnown_for_department() {
        return known_for_department;
    }

    public void setKnown_for_department(String known_for_department) {
        this.known_for_department = known_for_department;
    }

    public String getPlace_of_birth() {
        return place_of_birth;
    }

    public void setPlace_of_birth(String place_of_birth) {
        this.place_of_birth = place_of_birth;
    }

    public ArrayList<Profile> getProfiles() {
        return profiles;
    }

    public void setProfiles(ArrayList<Profile> profiles) {
        this.profiles = profiles;
    }

    public void getPersonsModels() {
        apiConnection.getPersonsList();
    }

    public void getPersonInfo(long personId) {
        apiConnection.getPersonInfo(personId);
    }

    @Override
    public void passPersonsModels(ArrayList<PersonModel> personModels) {
        iPersonModel.setPersonModels(personModels);
    }

    @Override
    public void passPersonModel(PersonModel personModel) {
        iPersonModel.setPersonInfo(personModel);
    }

    public void setIPersonModel(IPersonModel iPersonModel) {
        this.iPersonModel = iPersonModel;
    }

    public interface IPersonModel {
        public void setPersonModels(ArrayList<PersonModel> personModels);
        public void setPersonInfo(PersonModel personInfo);
    }
}
