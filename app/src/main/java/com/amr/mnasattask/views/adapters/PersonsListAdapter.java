package com.amr.mnasattask.views.adapters;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amr.mnasattask.R;
import com.amr.mnasattask.models.PersonModel;
import com.amr.mnasattask.utilities.EndPoints;
import com.amr.mnasattask.views.activities.DetailsActivity;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class PersonsListAdapter extends RecyclerView.Adapter<PersonsListAdapter.PersonItem> {

    private Context context;
    private ArrayList<PersonModel> personModels;

    public PersonsListAdapter(Context context, ArrayList<PersonModel> personModels) {
        this.context = context;
        this.personModels = personModels;
    }

    @NonNull
    @Override
    public PersonItem onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new PersonItem(LayoutInflater.from(context).inflate(R.layout.item_person, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final PersonItem personItem, int i) {
        Glide.with(context)
                .load(EndPoints.PERSON_MAIN_IMAGE + personModels.get(i).getProfile_path())
                .into(personItem.profile_path);
        personItem.name.setText(personModels.get(i).getName());

        personItem.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailsActivity.class);
                intent.putExtra("personId", personModels.get(personItem.getAdapterPosition()).getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return personModels == null ? 0 : personModels.size();
    }

    public class PersonItem extends RecyclerView.ViewHolder {
        private ImageView profile_path;
        private TextView name;
        private CardView item;

        public PersonItem(View view) {
            super(view);

            item = (CardView) view.findViewById(R.id.item);
            profile_path = (ImageView) view.findViewById(R.id.profile_path);
            name = (TextView) view.findViewById(R.id.name);
        }

    }
}
