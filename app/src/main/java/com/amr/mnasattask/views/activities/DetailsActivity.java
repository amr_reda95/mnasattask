package com.amr.mnasattask.views.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.amr.mnasattask.R;
import com.amr.mnasattask.views.fragments.PersonDetailsFragment;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Intent intent = getIntent();
        long personId = intent.getExtras().getLong("personId");

        PersonDetailsFragment personDetailsFragment = new PersonDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putLong("personId", personId);
        personDetailsFragment.setArguments(bundle);

        getSupportFragmentManager().beginTransaction().replace(R.id.personDetailsFragment, personDetailsFragment).commit();
    }
}
