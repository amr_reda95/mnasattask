package com.amr.mnasattask.views.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amr.mnasattask.R;
import com.amr.mnasattask.models.PersonModel;
import com.amr.mnasattask.utilities.EndPoints;
import com.bumptech.glide.Glide;

public class DetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public final int VIEW_TYPE_1 = 1, VIEW_TYPE_2 = 2;

    private Context context;
    private PersonModel personModel;

    public DetailsAdapter(Context context, PersonModel personModel) {
        this.context = context;
        this.personModel = personModel;
    }

    public boolean isHeader(int position) {
        return position == 0;
    }

    @Override
    public int getItemViewType(int position) {
        return isHeader(position) ? VIEW_TYPE_1 : VIEW_TYPE_2;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (viewType == VIEW_TYPE_1) {
            return new PersonMainInfo(LayoutInflater.from(context).inflate(R.layout.item_person_main_info, viewGroup, false));
        } else {
            return new PersonImages(LayoutInflater.from(context).inflate(R.layout.item_person_image, viewGroup, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        if (isHeader(holder.getAdapterPosition())) {
            Glide.with(context).load(EndPoints.PERSON_MAIN_IMAGE + personModel.getProfile_path()).into(((PersonMainInfo) holder).profile_path);
            ((PersonMainInfo) holder).name.setText(personModel.getName());
            ((PersonMainInfo) holder).birthday.setText(personModel.getBirthday());
            ((PersonMainInfo) holder).known_for_department.setText(personModel.getKnown_for_department());
            ((PersonMainInfo) holder).place_of_birth.setText(personModel.getPlace_of_birth());
        } else {
            Glide.with(context).load(
                    EndPoints.PERSON_MAIN_IMAGE + personModel.getProfiles().get(holder.getAdapterPosition() - 1).getFile_path())
                    .into(((PersonImages) holder).file_path
                    );
        }
    }

    @Override
    public int getItemCount() {
        return personModel.getProfiles().size() + 1;
    }

    class PersonMainInfo extends RecyclerView.ViewHolder {

        private ImageView profile_path;
        private TextView name, birthday, known_for_department, place_of_birth;

        public PersonMainInfo(View itemView) {
            super(itemView);

            profile_path = (ImageView) itemView.findViewById(R.id.profile_path);
            name = (TextView) itemView.findViewById(R.id.name);
            birthday = (TextView) itemView.findViewById(R.id.birthday);
            known_for_department = (TextView) itemView.findViewById(R.id.known_for_department);
            place_of_birth = (TextView) itemView.findViewById(R.id.place_of_birth);
        }
    }

    class PersonImages extends RecyclerView.ViewHolder {
        private ImageView file_path;

        public PersonImages(View itemView) {
            super(itemView);

            file_path = (ImageView) itemView.findViewById(R.id.file_path);
        }
    }
}
