package com.amr.mnasattask.views.adapters;

import android.content.Context;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.amr.mnasattask.R;
import com.amr.mnasattask.models.PersonModel;
import com.amr.mnasattask.utilities.EndPoints;
import com.bumptech.glide.Glide;


public class PersonDetailsAdapter extends SectionedRecyclerViewAdapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_1 = 1, VIEW_TYPE_2 = 2;

    private Context context;
    private PersonModel personModel;

    public PersonDetailsAdapter(Context context, PersonModel personModel) {
        this.context = context;
        this.personModel = personModel;
    }

//    @Override
//    public int getItemViewType(int section, int relativePosition, int absolutePosition) {
//        if (absolutePosition == 0) {
//            return VIEW_TYPE_1;
//        } else {
//            return VIEW_TYPE_2;
//        }
//    }

    @Override
    public int getSectionCount() {
        return 1;
    }

    @Override
    public int getItemCount(int section) {
        return personModel.getProfiles().size() + 1;
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int section) {
        if (holder.getItemViewType() == VIEW_TYPE_HEADER) {
            Glide.with(context).load(EndPoints.PERSON_MAIN_IMAGE + personModel.getProfile_path()).into(((PersonMainInfo) holder).profile_path);
            ((PersonMainInfo) holder).name.setText(personModel.getName());
            ((PersonMainInfo) holder).birthday.setText(personModel.getBirthday());
            ((PersonMainInfo) holder).known_for_department.setText(personModel.getKnown_for_department());
            ((PersonMainInfo) holder).place_of_birth.setText(personModel.getPlace_of_birth());
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int section, int relativePosition, int absolutePosition) {
        if (holder.getItemViewType() == VIEW_TYPE_1) {

        } else {

        }

        if (holder.getItemViewType() != VIEW_TYPE_HEADER) {
            Glide.with(context).load(
                    EndPoints.PERSON_MAIN_IMAGE + personModel.getProfiles().get(0).getFile_path())
                    .into(((PersonImages) holder).file_path
                    );
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        if (i == VIEW_TYPE_HEADER) {
            return new PersonMainInfo(LayoutInflater.from(context).inflate(R.layout.item_person_main_info, viewGroup, false));
        } else {
            return new PersonImages(LayoutInflater.from(context).inflate(R.layout.item_person_image, viewGroup, false));
        }
    }

    public class PersonMainInfo extends RecyclerView.ViewHolder {

        private ImageView profile_path;
        private TextView name, birthday, known_for_department, place_of_birth;

        public PersonMainInfo(View itemView) {
            super(itemView);

            profile_path = (ImageView) itemView.findViewById(R.id.profile_path);
            name = (TextView) itemView.findViewById(R.id.name);
            birthday = (TextView) itemView.findViewById(R.id.birthday);
            known_for_department = (TextView) itemView.findViewById(R.id.known_for_department);
            place_of_birth = (TextView) itemView.findViewById(R.id.place_of_birth);
        }
    }

    public class PersonImages extends RecyclerView.ViewHolder {
        private ImageView file_path;

        public PersonImages(View itemView) {
            super(itemView);

            file_path = (ImageView) itemView.findViewById(R.id.file_path);
        }
    }
}