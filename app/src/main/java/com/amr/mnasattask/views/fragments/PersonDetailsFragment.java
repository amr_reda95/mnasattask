package com.amr.mnasattask.views.fragments;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.amr.mnasattask.R;
import com.amr.mnasattask.databinding.FragmentPersonDetailsBinding;
import com.amr.mnasattask.databinding.FragmentPersonsBinding;
import com.amr.mnasattask.viewmodels.PersonDetailsViewModel;
import com.amr.mnasattask.viewmodels.PersonsListViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class PersonDetailsFragment extends Fragment {

    private RecyclerView personsList;
    private PersonDetailsViewModel personDetailsViewModel;
    private long personId;

    public PersonDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = binding(inflater, container);

        setUpUIElements(v);

        personId = getArguments().getLong("personId");

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        personDetailsViewModel.getData(personId);
    }

    private View binding(LayoutInflater inflater, ViewGroup container) {
        FragmentPersonDetailsBinding binding
                = DataBindingUtil.inflate(inflater, R.layout.fragment_person_details, container, false);
        personDetailsViewModel = new PersonDetailsViewModel(getActivity());
        binding.setPersonDetailsViewModel(personDetailsViewModel);

        return binding.getRoot();
    }

    private void setUpUIElements(View v) {
        personsList = (RecyclerView) v.findViewById(R.id.personInfo);
        personsList.setLayoutManager(
                new GridLayoutManager(getActivity(), 3)
        );
    }

}
