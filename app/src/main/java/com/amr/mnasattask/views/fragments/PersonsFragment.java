package com.amr.mnasattask.views.fragments;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.amr.mnasattask.databinding.FragmentPersonsBinding;

import com.amr.mnasattask.R;
import com.amr.mnasattask.viewmodels.PersonsListViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class PersonsFragment extends Fragment {

    private RecyclerView personsList;
    private PersonsListViewModel personsListViewModel;

    public PersonsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = binding(inflater, container);

        setUpUIElements(v);

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        personsListViewModel.getData();
    }

    private View binding(LayoutInflater inflater, ViewGroup container) {
        FragmentPersonsBinding binding
                = DataBindingUtil.inflate(inflater, R.layout.fragment_persons, container, false);
        personsListViewModel = new PersonsListViewModel(getActivity());
        binding.setPersonsListViewModel(personsListViewModel);

        return binding.getRoot();
    }

    private void setUpUIElements(View v) {
        personsList = (RecyclerView) v.findViewById(R.id.personsList);
        personsList.setLayoutManager(new GridLayoutManager(getActivity(),3));
    }

}
