package com.amr.mnasattask.utilities;

public class EndPoints {
    private static String BASE_URL = "https://api.themoviedb.org/3/person/";

    public static String PERSONS_LIST_URL = BASE_URL + "popular?api_key=";
    public static String PERSON_MAIN_INFO = BASE_URL + "person_id?api_key=";
    public static String PERSON_Images = BASE_URL + "person_id/images?api_key=";
    public static String PERSON_MAIN_IMAGE = "https://image.tmdb.org/t/p/w200";
}
